call pathogen#infect()
syntax on
filetype plugin indent on

set shell=/bin/bash

set nocompatible
set et
set sw=8
set sts=8
set smarttab
set cindent
set fdm=syntax
set foldlevelstart=20
set number
set nowrap

set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
"set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
"set undofile


set backupdir=$TEMP//,/tmp
set directory=$TEMP//,/tmp


let mapleader = ","


syntax enable
set background=dark
colorscheme gruvbox

set t_Co=256

if has('gui_running')
  set guifont=Consolas\ 11
  "set guifont=Ubuntu\ Mono\ 11
endif



"bracket completion
"inoremap {      {}<Left>
"inoremap {<CR>  {<CR>}<Esc>O
"inoremap {}     {}



:nnoremap <F5> :buffers<CR>:buffer<Space>
:nnoremap <leader>nt :NERDTree<cr>


nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk



inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>



function! NumberToggle()
  if(&relativenumber == 1)
    set number
  else
    set relativenumber
  endif
endfunc

set relativenumber

nnoremap <leader>n :call NumberToggle()<cr>


""Splits
nnoremap <leader>w <C-w>v<C-w>l

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l




"Search Stuff
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch



let g:html_indent_inctags = "html,body,head,tbody"
let g:html_indent_script1 = "inc"
let g:html_indent_style1 = "inc"


" syntastic
let g:syntastic_auto_loc_list=0
let g:syntastic_disabled_filetypes=['html']
let g:syntastic_enable_signs=1

"vim-go
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

let g:go_fmt_autosave = 0

